const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create', (request, response) => {
		TaskController.createTask(request.body).then((result) => {
			response.send(result)
		})
})

// GET all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// MINI ACTIVITY (30 mins.)
// Delete task
/*
	1. Setup a route for deleting a task
	2. Setup the controller for deleting a task
	3. Build the controller function responsible for deleting a task
	4. Send the result of the controller function as response

	Endpoint: '/:id/delete'
*/
router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})


// ACTIVITY

router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result) => {
		response.send(result)
	})
})

router.put('/:id/complete', (request, response) => {
	TaskController.updateSpecificTask(request.params.id).then((result) => {
		response.send(result)
	})
})

module.exports = router