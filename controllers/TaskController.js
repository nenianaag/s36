const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id) => {
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
		if(!removedTask){
			return 'Task not found'
		}

		return removedTask
	})
}
/* OR: 
module.exports.deleteTask = (task_id) => {
	return Task.DeleteOne({_id: task_id}).then((deletedTask, error) => {
		if(error){
			console.log(error)
			return error
		}

		return deletedTask
	})
}
*/

// ACTIVITY
module.exports.getSpecificTask = (task_id) => {
	return Task.findById(task_id).then((retrievedTask, error) => {
		if(error) {
			return false
		} else {
			return retrievedTask
		}
		
	})
}

module.exports.updateSpecificTask = (task_id) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.status = "complete"

		return result.save().then((updatedStatusTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedStatusTask
		})
	})
}

